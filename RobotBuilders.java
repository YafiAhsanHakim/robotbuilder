import java.io.IOException;

import Action.ActionProcessor;
import Builder.Builder;
import ManualPrinter.ManualPrinter;


public class RobotBuilders {
    
    private static Builder builder;
    private static ActionProcessor actionProcessor;
    private static ManualPrinter manualPrinter = new ManualPrinter();
    private static String status;

	public static void main(String[] args) throws IOException {
        
        System.out.println("\n=============================");
        System.out.println(" Welcome to RobotBuilders!! ");
        System.out.println("=============================\n");

        do {
            builder = new Builder();
            builder.start();
            manualPrinter.print(builder.getRobot());
            actionProcessor = new ActionProcessor(builder.getRobot());
            status = "command";
            do {
                status = actionProcessor.run("user");
                if (status.equals("help")) { 
                    manualPrinter.print(builder.getRobot());
                    status = "command";
                }
            }while(status.equals("command"));
        }while(status.equals("reset"));
        
    }
}