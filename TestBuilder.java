import java.io.IOException;

import Builder.Builder;
import ManualPrinter.ManualPrinter;

public class TestBuilder {
    
    public static void main(String[] args) throws IOException { 

        Builder builder = new Builder();
        ManualPrinter manualPrinter = new ManualPrinter(); 

        builder.inputPart("test", "head");
        builder.inputPart("test", "arm");
        builder.inputPart("test", "leg");
        System.out.println("robot created.");
        
        try {
            System.out.println("check head."); 
            builder.getRobot().getHead();
            System.out.println("check arm."); 
            builder.getRobot().getArm();
            System.out.println("check leg.\n"); 
            builder.getRobot().getLeg(); 

            manualPrinter.print(builder.getRobot());

            System.out.println("test success.");   
        } catch(NullPointerException e) { 
            System.out.println("test failed.");
        }
    }
}