package Builder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import Builder.Parts.*;
import InputReader.InputReader;

public class Builder {
    private Robot robot;
    private InputReader in;
    private boolean valid;
    
    public Builder(){
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        robot = new Robot();
    }

    public Robot getRobot() {
        return this.robot;
    }

    public void start() throws IOException {
        System.out.println("Head Parts: \n 1. XrayGoogles \n 2. LaserEyes");
        System.out.print("Choose your head (input the number of the option) : ");
        do {
            valid = inputPart("user","head");
        }while(!valid);

        System.out.println("Arm Parts: \n 1. RocketLauncher \n 2. PlasmaSword");
        System.out.print("Choose your arm (input the number of the option) : ");
        do {
            valid = inputPart("user","arm");
        }while(!valid);

        System.out.println("Leg Parts: \n 1. FloatingMachine \n 2. JumpingLeg");
        System.out.print("Choose your legs (input the number of the option) : ");
        do {
            valid = inputPart("user","leg");
        }while(!valid);

        System.out.println("Your robot is completed!\n");
    }

    public boolean inputPart(String inputType, String part) throws IOException {
        String inp = "";
        if (inputType.equals("test")) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("TestBuilderInput.txt"));
            String[] data = new String[3];

            for (int i=0; i<3; i++) {
                data[i] = bufferedReader.readLine();
            }
            bufferedReader.close();

            if (part.equals("head")) {
                inp = data[0]; 
            } else if (part.equals("arm")) {
                inp = data[1]; 
            } else {
                inp = data[2]; 
            }
        } else {
            inp = in.next(); 
        }
        
        if (part.equals("head")) {
            return chooseHead(inp); 
        } else if (part.equals("arm")) {
            return chooseArm(inp);
        } else {
            return chooseLeg(inp); 
        } 
    }
    
    public boolean chooseHead(String inp) {
        Parts part = new BasicPart("template");
        if (inp.equals("1")) {
            part.setName("X-ray Googles");
            part = new ToggleDecorator(part, "see through walls");
            part.setCommand("toggle", "xray", "can see through walls");
        
        } else if (inp.equals("2")) {
            part.setName("Laser Eyes");
            part.addCommand("laser", "shoots a laser from the eye", "your robot shoots a laser! it makes a hole through a wall!");  
        
        } else {
            System.out.println("invalid input");
            return false;
        }
        robot.setHead(part);
        System.out.println("\nequipped " + part.getName() + " to the head!\n");
        return true;
    }

    public boolean chooseArm(String inp) {
        Parts part = new BasicPart("template");
        if (inp.equals("1")) {
            part.setName("Rocket Launcher");
            part = new AmmoDecorator(part, 3);
            part.setCommand("shoot", "shoot", "shoots a rocket");
            part.setCommand("reload", "reload", "reloads the ammo for the rocket launcher");
        
        } else if (inp.equals("2")) {
            part.setName("Plasma Sword");
            part.addCommand("slash", "slashes with it's plasma sword hand", "your robot slashes with it's plasma sword, it cuts a steel bar in half!");
        
        } else {
            System.out.println("invalid input");
            return false;
        }
        robot.setArm(part);
        System.out.println("\nequipped " + part.getName() + " as the arm!\n");
        return true;
    }

    public boolean chooseLeg(String inp) {
        Parts part = new BasicPart("template");
        if (inp.equals("1")) {
            part.setName("Floating Machine");
            part = new StateDecorator(part, new String[]{ "ground","sky","space" });
            part.setCommand("next","ascend", "can see through walls");
            part.setCommand("previous","descend", "can see through walls");
            part.setTexts(new String[] {"ascend", "descend", "is currently in"});
        
        } else if (inp.equals("2")) {
            part.setName("Jumping Leg");
            part.addCommand("jump", "jumps", "your robot jumps over a building!!");
        
        } else {
            System.out.println("invalid input");
            return false;
        }
        robot.setLeg(part);
        System.out.println("\nequipped " + part.getName() + " as the leg!\n");
        return true;
    }
}