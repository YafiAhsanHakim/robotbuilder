package Builder.Parts;

public class StateDecorator extends BaseDecorator{

    private String[] states;
    private String[] descriptions;
    private int state;
    private String nextInput;
    private String previousInput;

    public StateDecorator(Parts part, String[] states) {
        super(part.getName(), part);
        this.states = states;
        this.state = 0;
    }

    @Override
    public void setTexts(String[] descs) {
        descriptions = descs;
    }

    @Override
    public void setCommand(String type, String input, String command) {
        this.commands.add(input + " : your robot " + command + ".");
        this.inputs.add(input);
        if (type.equals("next")) {
            nextInput = input;
        } else {
            previousInput = input;
        }
    }

    @Override
    public String command(String commandName){
        if (commandName.equals(nextInput)){
            return next();
        } else if (commandName.equals(previousInput)){
            return previous();
        } else {
            return this.texts.get(commandName);
        }
    }

    public String next(){
        if (state == (states.length-1)) {
            return "your robot can't " + descriptions[0] + " any more! your robot " + descriptions[2] + " " + states[state] + ".";
        } else {
            state++;
            return "your robot " + descriptions[0] + "! your robot " + descriptions[2] + " " + states[state] + ".";
        }
    } 

    public String previous(){
        if (state == 0) {
            return "your robot can't " + descriptions[1] + " any more! your robot " + descriptions[2] + " " + states[state] + ".";
        } else {
            state--;
            return "your robot " + descriptions[1] + "! your robot " + descriptions[2] + " " + states[state] + ".";
        }
    } 
}