package Action;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import Builder.Robot;
import InputReader.InputReader;

public class ActionProcessor {
    private Robot robot;
    private static InputReader in; 

    public ActionProcessor(Robot robot) {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        this.robot = robot;
    }

    public String run(String inputType) throws IOException {
        String inp = "";
        if (inputType.equals("test")) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("TestActionInput.txt"));
            for (int i=0; i<6; i++) {
                inp = bufferedReader.readLine();
                System.out.print("Command your robot to do actions! : " + inp + "\n");
                process(inp);
            }
            bufferedReader.close();
            return inp;
        } else {
            System.out.print("Command your robot to do actions! : ");
            inp = in.next();
            return process(inp); 
        } 
    }

    public String process(String inp) throws IOException {
		if (inp.equals("reset")) {
			return "reset";
		} else if (inp.equals("exit")) {
			return "exit";
		} else if (inp.equals("help")) {
			return "help";
		} else if (robot.getCommandList().containsKey(inp)){
            System.out.println(robot.getCommandList().get(inp).command(inp));
            System.out.println("");
            return "command";
		} else {
            System.out.println("your robot doesn't have the right part to run that command!");
            System.out.println("");
			return "command";
		}
    }
}