package Builder.Parts;

public abstract class BaseDecorator extends Parts {
    
    protected Parts part;

    public BaseDecorator(String name, Parts part) {
        super(name);
        this.part = part;
    }
}