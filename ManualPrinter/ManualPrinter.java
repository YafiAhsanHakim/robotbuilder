package ManualPrinter;

import Builder.Robot;

public class ManualPrinter {
    
    public ManualPrinter() {}

    public void print(Robot robot) {
        System.out.println("====================:MANUAL:=============================");
        for (String i : robot.getHead().getCommands()){
            System.out.println(" " + i);
        }
        for (String j : robot.getArm().getCommands()){
            System.out.println(" " + j);
        }
        for (String k : robot.getLeg().getCommands()){
            System.out.println(" " + k);
        }
        System.out.println(" reset : destroy your current robot and create a new one.");
        System.out.println(" exit : quits the RobotBuilder.");
        System.out.println(" \n help : shows the manual again.");
        System.out.println("=========================================================\n");
    }
}