package Builder.Parts;

public class BasicPart extends Parts{

    public BasicPart(String name) {
        super(name);
    }

    @Override
    public String command(String commandName) {
        return this.texts.get(commandName);
    }
}
