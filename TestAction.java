import java.io.IOException;

import Action.ActionProcessor;
import Builder.Builder;

public class TestAction {
    
    public static void main(String[] args) throws IOException { 
        Builder builder = new Builder();
        builder.inputPart("test", "head");
        builder.inputPart("test", "arm");
        builder.inputPart("test", "leg");
        System.out.println("robot created.");

        ActionProcessor actionProcessor = new ActionProcessor(builder.getRobot());
        actionProcessor.run("test");
    }
}