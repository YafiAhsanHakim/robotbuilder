package Builder.Parts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Parts {
    
    protected String name;
    protected List<String> commands = new ArrayList<>();
    protected List<String> inputs = new ArrayList<>();
    protected HashMap<String, String> texts = new HashMap<>();

    public Parts(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    } 

    public String getName() {
        return this.name;
    }

    public void setCommand(String type, String input, String command) {}

    public void setTexts(String[] descs) {}

    public List<String> getCommands() {
        return this.commands;
    }

    public List<String> getInputs() {
        return this.inputs;
    }

    public HashMap<String, String> getTexts() {
        return this.texts;
    }

    public void addCommand(String input, String command, String description) {
        this.commands.add(input + " : your robot " + command + ".");
        this.inputs.add(input);
        this.texts.put(input, description);
    }

    public abstract String command(String commandName);
}