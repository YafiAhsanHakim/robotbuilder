package Builder;

import java.util.HashMap;
import Builder.Parts.Parts;

public class Robot {
    private Parts head, arm, leg;
    private HashMap<String, Parts> commandList = new HashMap<>();

    public Robot() {}

    public void setHead(Parts head){
        this.head = head;
        for (String commandName : head.getInputs()){
            commandList.put(commandName, head);
        }
    }
    public void setArm(Parts arm){
        this.arm = arm;
        for (String commandName : arm.getInputs()){
            commandList.put(commandName, arm);
        }
    }
    public void setLeg(Parts leg){
        this.leg = leg;
        for (String commandName : leg.getInputs()){
            commandList.put(commandName, leg);
        }
    }
    
    public Parts getHead(){
        return this.head;
    }
    public Parts getArm(){
        return this.arm;
    }
    public Parts getLeg(){
        return this.leg;
    }
    public HashMap<String, Parts> getCommandList(){
        return this.commandList;
    }
}