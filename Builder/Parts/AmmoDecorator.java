package Builder.Parts;

public class AmmoDecorator extends BaseDecorator{

    private int ammo;
    private int maxAmmo;
    private String shootInput;
    private String reloadInput;

    public AmmoDecorator(Parts part, int ammo) {
        super(part.getName(), part);
        this.ammo = ammo;
        this.maxAmmo = ammo;
    }

    @Override
    public void setCommand(String type, String input, String command) {
        this.commands.add(input + " : your robot " + command + ".");
        this.inputs.add(input);
        if (type.equals("shoot")) {
            shootInput = input;
        } else {
            reloadInput = input;
        }
    }

    @Override
    public String command(String commandName) {
        if (commandName.toLowerCase().equals(this.shootInput)) {
            return shoot();
        } else if (commandName.toLowerCase().equals(this.reloadInput)) {
            return reload();
        } else {
            return this.texts.get(commandName);
        }
    }

    public String shoot(){
        if (ammo > 0) {
            ammo--;
            return "your robot shoots from it's " + this.name + "! (current ammo : " + ammo + ")";
        } else {
            return "your " + this.name + " is out of ammo! reload first before you shoot! (current ammo : 0)";
        }
    } 

    public String reload(){
        this.ammo = maxAmmo;
        return this.name + " reload complete. (current number of ammo : " + ammo + ")";
    } 
}