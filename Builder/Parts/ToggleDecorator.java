package Builder.Parts;

public class ToggleDecorator extends BaseDecorator{

    private boolean status = false;
    private String toggleInput;
    private String function;

    public ToggleDecorator(Parts part, String function) {
        super(part.getName(), part);
        this.function = function;
    }

    @Override
    public void setCommand(String type, String input, String command) {
        this.commands.add(input + " : your robot " + command + ".");
        this.inputs.add(input);
        if (type.equals("toggle")) {
            toggleInput = input;
        }
    }

    @Override
    public String command(String commandName) {
        if (commandName.equals(toggleInput)) {
            return toogle();
        } else {
            return this.texts.get(commandName);
        }
        
    }

    public String toogle(){
        if (status == false) {
            status = true;
            return part.getName() + " turned on, your robot " + this.function + "!";
        } else {
            status = false;
            return part.getName() + " turned off.";
        }
    }
}