Documentation : https://docs.google.com/document/d/1IrJ6-ViW6kktNJfQMBl6Ofxl8ISBrAomioh7YhfY4zA/edit?usp=sharing
Works log : https://docs.google.com/spreadsheets/d/1bY5N41VL7l9MRs_OWlApJGPhP09mRRaHhFPGncS3GTY/edit?usp=sharing

INSTRUCTION TO RUN:
1. run RobotBuilder.java
2. you will be asked to choose the head, input the number of the option to choose
3. you will be asked to choose the arm, input the number of the option to choose
4. you will be asked to choose the leg, input the number of the option to choose
5. the robot is completed
6. a manual will then be displayed
7. you can then make the robot do certain actions using the command listed in the manual
8. you can exit the program by inputting "exit" command

DEFAULT COMMANDS:
1. "exit"   : quit the app
2. "reset"  : create a new robot
3. "help"   : show the manual again
